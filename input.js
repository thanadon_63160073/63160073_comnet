function checkIp() {
    var ip = document.getElementById("cip").value;
    var dotIp = ip.split(".");
    if (dotIp.length != 4) {
        alert("Please provide a IP address with enougth length.");
        document.getElementById('cip').focus();
        return false;
    }
    for (let i = 0; i < 4; i++) {
        dotIp[i] = parseInt(dotIp[i]);
        if (dotIp[i] < 0 || dotIp[i] > 255) {
            alert("Please provide a valid IP address.");
            document.getElementById('cip').focus();
            return false;
        }
    }
    return true;
}
function clearForm() {
    document.getElementById('cip').value = "";
    document.getElementById('cip').focus();
}
csubnet = '30'
function setSubnetV(inval) {
    csubnet = inval;
    return false;
}

function popSubnet(inval) {
    var lowerlimit = 0;
    if (inval == 'a') lowerlimit = 8;
    if (inval == 'b') lowerlimit = 16;
    if (inval == 'c') lowerlimit = 24;

    function calSubnet(bit) {
        cal_snet = '0';
        if (bit_snet == 7) {
            cal_snet = '254'
        } else if (bit_snet == 6) {
            cal_snet = '252'
        } else if (bit_snet == 5) {
            cal_snet = '248'
        } else if (bit_snet == 4) {
            cal_snet = '240'
        } else if (bit_snet == 3) {
            cal_snet = '224'
        } else if (bit_snet == 2) {
            cal_snet = '192'
        } else if (bit_snet == 1) {
            cal_snet = '128'
        }
        return cal_snet;
    }
    var allSubnets = [];
    for (let i = 0; i <= 32; i++) {
        var bit_snet = i % 8;
        var cnet = (i - bit_snet) / 8;
        var snet_mask = '';
        var cal_snet = '0';
        switch (cnet) {
            case 1:
                cal_snet = calSubnet(bit_snet);
                snet_mask = '255.' + cal_snet + '.0.0 /' + i;
                break;
            case 2:
                cal_snet = calSubnet(bit_snet);
                snet_mask = '255.255.' + cal_snet + '.0 /' + i;
                break;
            case 3:
                cal_snet = calSubnet(bit_snet);
                snet_mask = '255.255.255.' + cal_snet + ' /' + i;
                break;
            case 4:
                snet_mask = '255.255.255.255 /' + i;
                break;
        }
        allSubnets.push([i, snet_mask]);
    }
    var thisSelect = document.getElementById('csubnet');
    thisSelect.options.length = 0;
    for (i = 0; i < allSubnets.length; i++) {
        if (allSubnets[i][0] >= lowerlimit) {
            var thisOption = document.createElement('option');
            thisOption.value = allSubnets[i][0];
            thisOption.innerHTML = allSubnets[i][1];
            if (csubnet == allSubnets[i][0]) thisOption.selected = true;
            thisSelect.appendChild(thisOption);
        }
    }
}
popSubnet('a');