window.addEventListener('load', () => {
    const params = (new URL(document.location)).searchParams;
    const cip = params.get('cip');
    const csubnet = parseInt(params.get('csubnet'));
    const cclass = params.get('cclass');
    var subClass = 'any';
    if (csubnet < 16) {
        subClass = 'A';
    } else if (csubnet < 24) {
        subClass = 'B';
    } else {
        subClass = 'C';
    }


    function calSubnet(bit) {
        cal_snet = '0';
        if (bit_snet == 7) {
            cal_snet = '254'
        } else if (bit_snet == 6) {
            cal_snet = '252'
        } else if (bit_snet == 5) {
            cal_snet = '248'
        } else if (bit_snet == 4) {
            cal_snet = '240'
        } else if (bit_snet == 3) {
            cal_snet = '224'
        } else if (bit_snet == 2) {
            cal_snet = '192'
        } else if (bit_snet == 1) {
            cal_snet = '128'
        }
        return cal_snet;
    }
    var allSubnets = [];
    for (let i = 0; i <= 32; i++) {
        var bit_snet = i % 8;
        var cnet = (i - bit_snet) / 8;
        var snet_mask = '';
        var cal_snet = '0';
        switch (cnet) {
            case 1:
                cal_snet = calSubnet(bit_snet);
                snet_mask = '255.' + cal_snet + '.0.0 /' + i;
                break;
            case 2:
                cal_snet = calSubnet(bit_snet);
                snet_mask = '255.255.' + cal_snet + '.0 /' + i;
                break;
            case 3:
                cal_snet = calSubnet(bit_snet);
                snet_mask = '255.255.255.' + cal_snet + ' /' + i;
                break;
            case 4:
                snet_mask = '255.255.255.255 /' + i;
                break;
        }
        allSubnets.push([i, snet_mask]);
    }
    function intIpToBinaryIp(intIp) {
        var v = intIp.split(".");
        for (let i = 0; i < 4; i++) {
            if (parseInt(v[i]) < 2) {
                v[i] = '0000000' + parseInt(v[i]).toString(2);
            } else if (parseInt(v[i]) < 4) {
                v[i] = '000000' + parseInt(v[i]).toString(2);
            } else if (parseInt(v[i]) < 8) {
                v[i] = '00000' + parseInt(v[i]).toString(2);
            } else if (parseInt(v[i]) < 16) {
                v[i] = '0000' + parseInt(v[i]).toString(2);
            } else if (parseInt(v[i]) < 32) {
                v[i] = '000' + parseInt(v[i]).toString(2);
            } else if (parseInt(v[i]) < 64) {
                v[i] = '00' + parseInt(v[i]).toString(2);
            } else if (parseInt(v[i]) < 128) {
                v[i] = '0' + parseInt(v[i]).toString(2);
            } else {
                v[i] = parseInt(v[i]).toString(2);
            }
        }
        return v[0] + "." + v[1] + "." + v[2] + "." + v[3];
    }
    function getWip(sip) {
        var v = sip.split(".");
        for (let i = 0; i < 4; i++) {
            v[i] = 255 - parseInt(v[i]);
        }
        return v[0] + "." + v[1] + "." + v[2] + "." + v[3];
    }
    function getTotalHost(wip) {
        var v = wip.split(".");
        for (let i = 0; i < 4; i++) {
            v[i] = parseInt(v[i]) + 1;
        }
        return v[0] * v[1] * v[2] * v[3];
    }
    function getNetIP(sip, ip) {
        var ip1 = sip.split('.');
        var ip2 = ip.split('.');
        var netIp = [];
        for (let i = 0; i < 4; i++) {
            netIp.push(ip1[i] & ip2[i]);
        }
        return netIp[0] + "." + netIp[1] + "." + netIp[2] + "." + netIp[3];
    }
    function getBroadIP(wip, ip) {
        var ip1 = wip.split('.');
        var ip2 = ip.split('.');
        var broadIp = [];
        for (let i = 0; i < 4; i++) {
            broadIp.push(ip1[i] | ip2[i]);
        }
        return broadIp[0] + "." + broadIp[1] + "." + broadIp[2] + "." + broadIp[3];
    }
    function getUsableHostIp(netIp, broadIp) {
        var ip1 = netIp.split('.');
        var ip2 = broadIp.split('.');
        if (parseInt(ip2[3]) == parseInt(ip1[3])) {
            return 'NA';
        }
        ip1[3] = parseInt(ip1[3]) + 1;
        if (ip1[3] == parseInt(ip2[3])) {
            return 'NA';
        }
        ip2[3] = parseInt(ip2[3]) - 1;
        return ip1[0] + "." + ip1[1] + "." + ip1[2] + "." + ip1[3] + " - " + ip2[0] + "." + ip2[1] + "." + ip2[2] + "." + ip2[3];
    }
    function dotToNum(dot) {
        var d = dot.split('.');
        return ((((((+d[0]) * 256) + (+d[1])) * 256) + (+d[2])) * 256) + (+d[3]);
    }
    function notDotIp(dotIp) {
        for (let i = 0; i < 4; i++) {
            dotIp = dotIp.replace(".", "");
        }
        return dotIp;
    }
    function checkIpType(ip_addr) {
        var ip = ip_addr.split(".");
        if (ip[0] == '10') {
            return 'Private';
        }
        if (ip[0] == '192' && ip[1] == '168') {
            return 'Private';
        }
        if (ip[0] == '172') {
            for (let i = 16; i <= 31; i++) {
                if (ip[1] == i + '') {
                    return 'Private';
                }
            }
        }
        return 'Public';
    }

    //ผลลัพธ์ของตาราง
    var sip = allSubnets[csubnet][1].replace(' /' + csubnet, '');
    var wip = getWip(sip);
    var bsnet = intIpToBinaryIp(sip);
    var netIp = getNetIP(sip, cip);
    var broadIp = getBroadIP(wip, cip);
    document.getElementById('ip-addr').innerHTML = cip;
    document.getElementById('cidr').innerHTML = '/' + csubnet;
    document.getElementById('short').innerHTML = cip + ' /' + csubnet;
    document.getElementById('class_').innerHTML = subClass;
    document.getElementById('snet-mask').innerHTML = sip;
    document.getElementById('binary-snet').innerHTML = bsnet;
    document.getElementById('wnet-mask').innerHTML = wip;
    document.getElementById('totalHost').innerHTML = getTotalHost(wip);
    document.getElementById('usableHost').innerHTML = getTotalHost(wip) - 2 <= 0 ? 0 : getTotalHost(wip) - 2;
    document.getElementById('netIp').innerHTML = netIp;
    document.getElementById('broadIp').innerHTML = broadIp;
    document.getElementById('uhIp').innerHTML = getUsableHostIp(netIp, broadIp);
    document.getElementById('bId').innerHTML = notDotIp(intIpToBinaryIp(cip));
    document.getElementById('intId').innerHTML = dotToNum(cip);
    document.getElementById('ipType').innerHTML = checkIpType(cip);

    //เก็บ input ก่อนหน้าไว้
    document.getElementById('cip').value = cip;
    document.getElementById('csubnet').value = csubnet;
    document.forms["calform"][cclass].checked = true;

})